const {
    CI_REGISTRY_USER,
    CI_REGISTRY_PASSWORD,
    CI_REGISTRY,
    RENOVATE_DRY_RUN
} = process.env;

module.exports = {
    'dryRun': RENOVATE_DRY_RUN === 'true',
    'logFile': 'renovate.log',
    'logFileLevel': 'debug',
    'onboarding': true,
    'onboardingConfig': {
        'extends': [
            ':timezone(Europe/Moscow)',
            ':separateMajorReleases',
            ':combinePatchMinorReleases',
            ':ignoreUnstable',
            ':prNotPending',
            ':rebaseStalePrs',
            ':semanticCommits',
            ':semanticCommitType(build)',
            ':semanticCommitScope(deps)',
            ':updateNotScheduled',
            ':automergeMinor',
            ':automergeBranch',
            ':ignoreModulesAndTests',
            ':maintainLockFilesDisabled',
            ':autodetectPinVersions',
            ':disableRateLimiting',
            ':assignAndReview(savushkin.i)',
            'group:monorepos',
            'group:recommended',
            'helpers:disableTypesNodeMajor'
        ],
        'commitMessageTopic': '{{depName}}',
        'baseBranches': [
            'develop'
        ],
        'packageRules': [
            {
                'matchPackageNames': ['husky'],
                'separateMinorPatch': true
            },
            {
                'matchPackageNames': ['husky'],
                'matchUpdateTypes': ['major'],
                'enabled': false
            },
            {
                'matchPackageNames': ['typescript'],
                'separateMinorPatch': true
            },
            {
                'matchPackageNames': ['typescript'],
                'matchUpdateTypes': ['minor','major'],
                'automerge': false
            },
            {
                'matchDepTypes': ['peerDependencies'],
                'rangeStrategy': 'bump'
            },
            {
                'matchSourceUrlPrefixes': [
                    'https://github.com/angular/angular-cli',
                    'https://github.com/angular/angular'
                ],
                'groupName': 'Angular packages'
            }
        ]
    },
    'repositories': [
        'target-rus.ru/target-rus-frontend',
        'target-rus.ru/target-rus-backend',
        'ngx-library/ngx-library'
    ],
    'hostRules': [
        {
            'hostType': 'docker',
            'matchHost': CI_REGISTRY,
            'username': CI_REGISTRY_USER,
            'password': CI_REGISTRY_PASSWORD
        }
    ]
};
